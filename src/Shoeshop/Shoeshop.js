import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './dataShoe'
import ItemList from './ItemList'
import Itemshoe from './Itemshoe'
import ListShoe from './ListShoe'

export default class Shoeshop extends Component {
    state = {
        shoeArr: dataShoe, 
        detail: dataShoe[0], 
        cart: [],
    }
    handleChangeDetailShoe = (shoe) => {
        this.setState({
            detail:shoe,    
        })
    }
    handleAddToCart = (shoe) => {
        let cloneCart = [...this.state.cart]
        
        let index = this.state.cart.findIndex((item) => {
            return item.id ==  shoe.id;
        });
        if (index == -1) {
            let cartItem = {...shoe,number: 1}
            cloneCart.push(cartItem);
        } else {
            cloneCart[index].number++;
        }
        this.setState({cart: cloneCart});
    }
    handleChangeNumberMinus = (shoe) => {
      let cloneCart = [...this.state.cart]
        
      let index = this.state.cart.findIndex((item) => {
        return item.id == shoe.id;
      });
      console.log(shoe.number);
      cloneCart[index].number--;
      if (shoe.number <= 0) {
        cloneCart.splice(index,1);
      }
      this.setState({cart: cloneCart});
    }
  render() {
    console.log(this.state.shoeArr)
    return (
      <div className='container py-5'>
        <Cart
        handleChangeNumberMinus = {this.handleChangeNumberMinus}
        cart={this.state.cart}
        handleAddToCart = {this.handleAddToCart}
        />
        {/* <div className="row">{this.renderListShoe()}</div> */}
        <ListShoe
        handleAddToCart = {this.handleAddToCart}
        handleChangeDetailShoe={this.handleChangeDetailShoe}
        shoeArr= {this.state.shoeArr} />
        <ItemList  detail={this.state.detail} />
      </div>
    )
  }
}
