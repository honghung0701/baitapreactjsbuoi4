import React, { Component } from 'react'
import Itemshoe from './Itemshoe'

export default class ListShoe extends Component {
    renderListShoe = () => {
        return this.props.shoeArr.map((item,index) => {
            return <Itemshoe 
            handleAddToCart={this.props.handleAddToCart}
            handleChangeDetailShoe = {this.props.handleChangeDetailShoe}
            data={item}key={index}/>
        })
    }
  render() {
    return (
      <div>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    )
  }
}
