import React, { Component } from 'react'

export default class ItemList extends Component {
  render() {
    let { image, name, description, price, id }= this.props.detail;
    return (
      <div className='row mt-5 alert-secondary p-5 text-left'>
        <img className='col-3' src={image} alt="" />
        <div className='col-9'>
            <p>ID: {id}</p>
            <h5>Name: {name}</h5>
            <p>Desc: {description}</p>
            <p>Price: {price} $</p>
        </div>
      </div>
    )
  }
}
