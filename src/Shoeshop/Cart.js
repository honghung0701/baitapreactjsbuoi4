import React, { Component } from 'react'

export default class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td><img style={{width:"50px"}} src={item.image} alt="" /></td>
                <td>{item.price * item.number}$</td>
                <td>
                  <button 
                  onClick={() => {
                  this.props.handleChangeNumberMinus(item);
                }}
                  className='btn btn-danger'>-</button>
                  <span className='mx-2'>{item.number}</span>
                <button
                onClick={() => {
                  this.props.handleAddToCart(item);
                }}
                className='btn btn-success'>+</button>  
                </td>
            </tr>
        })
    }
  render() {
    return (
      <table className='table'>
        <thead>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Img</td>
                <td>Price</td>
                <td>Quanlity (Số lượng)</td>
            </tr>
        </thead>
        <tbody>
            {this.renderTbody()}
        </tbody>
      </table>
    )
  }
}
